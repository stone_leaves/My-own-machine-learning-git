# -*- coding: UTF-8 -*-
import numpy as np

"""
    利用numpy实现线性回归
    注：本代码中的异常名可能不太符合逻辑
"""


class LinearRegression:

    # 构造函数
    def __init__(self, fit_intercept=True):
        # 自变量的数量
        self.n_features = 0

        # 是否考虑截距项（不考虑则截距为0）
        self.fit_intercept = fit_intercept

        # 系数数组
        self.coef_ = np.array([])

        # 截距数组
        if self.fit_intercept:
            self.intercept = 0

    # 训练模型
    def fit(self, X, Y, axis=0):

        # 把输入数据全部转化成按行排列
        if len(X.shape) == 1:
            if axis == 0:
                x = np.array([X])
            elif axis == 1:
                x = X.reshape(-1, 1)
            else:
                raise IndexError
        else:
            if axis == 0:
                x = np.copy(X)
            elif axis == 1:
                x = X.transpose()
            else:
                raise IndexError
        y = Y.reshape(-1, 1)

        self.n_features = x.shape[1]
        n_sample = x.shape[0]

        # 是否有截距项决定是否要拼接一列全为0的矩阵元素
        if self.fit_intercept:
            x = np.hstack((np.ones([n_sample, 1]), x))

        # 计算参数
        arg = np.dot(np.dot(np.linalg.inv(np.dot(x.transpose(), x)), x.transpose()), y)

        if self.fit_intercept:
            self.coef_ = arg[1:]
            self.intercept = arg[0]
        else:
            self.coef_ = arg

    # 预测数据
    def predict(self, X, axis=0):
        if len(X.shape) == 1:
            if axis == 0:
                x = np.array([X])
            else:
                x = X.reshape(-1, 1)
        else:
            if axis == 0:
                x = np.copy(X)
            elif axis == 1:
                x = X.transpose()
            else:
                raise IndexError

        if x.shape[1] != self.n_features:
            raise IndexError
        if self.fit_intercept:
            x = np.hstack((np.ones([x.shape[0], 1]), x))
            if len(x.shape) == 1:
                x = np.array([x])
            arg = np.hstack((np.array([self.intercept]), self.coef_)).reshape(-1, 1)
            y = np.dot(x, arg)
        else:
            if len(x.shape) == 1:
                x = np.array([x])
            y = np.dot(x, self.coef_.reshape(-1, 1))
        return y
